import sys
import torch.cuda
from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension, CUDAExtension
from torch.utils.cpp_extension import CUDA_HOME

CXX_FLAGS = [] if sys.platform == 'win32' else ['-g', '-Werror', '-I', '/usr/local/cuda-9.0/include', '-L', '/usr/local/cuda-9.0/lib64']

ext_modules = []

if torch.cuda.is_available() and CUDA_HOME is not None:
    extension = CUDAExtension(
        'torch_cuda_strided_complex.cuda', [
            'CUDAComplexType.cpp',
            'cuda_extension_kernel.cu',
            'cuda_extension_kernel2.cu',
        ],
        extra_compile_args={'cxx': CXX_FLAGS,
                            'nvcc': ['-O2']})
    ext_modules.append(extension)


setup(
    name='torch_cuda_strided_complex',
    packages=['torch_cuda_strided_complex'],
    ext_modules=ext_modules,
    cmdclass={'build_ext': BuildExtension})



