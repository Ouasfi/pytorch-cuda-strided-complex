# pytorch-cuda-strided-complex

PyTorch extension that adds support for:

* device: cuda
* layout: strided
* dtype: complex64, complex128

## Install

```
git clone git@gitlab.com:pytorch-complex/pytorch-cuda-strided-complex.git
cd pytorch-cuda-strided-complex
rm -rf build/ && rm -rf dist/  # Always remove build and dist cached folders
python setup.py install
```

## Test

```
cd test
python test_torch.py
```